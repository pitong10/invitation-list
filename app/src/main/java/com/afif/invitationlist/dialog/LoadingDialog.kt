package com.afif.invitationlist.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.afif.invitationlist.databinding.FragmentLoadingDialogBinding

class LoadingDialog : DialogFragment() {

    lateinit var binding: FragmentLoadingDialogBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding = FragmentLoadingDialogBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
    }

    companion object {
        private var INSTANCE: LoadingDialog? = null

        @JvmStatic
        fun getInstance(): LoadingDialog {
            if (INSTANCE == null) {
                synchronized(LoadingDialog::javaClass) {
                    INSTANCE = LoadingDialog()
                }
            }
            return INSTANCE!!
        }

    }
}