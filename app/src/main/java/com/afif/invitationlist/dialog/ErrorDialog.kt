package com.afif.invitationlist.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.afif.invitationlist.databinding.FragmentErrorDialogBinding

class ErrorDialog : DialogFragment() {

    lateinit var binding: FragmentErrorDialogBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentErrorDialogBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    companion object {
        private var INSTANCE: ErrorDialog? = null

        @JvmStatic
        fun getInstance(): ErrorDialog {
            if (INSTANCE == null) {
                synchronized(ErrorDialog::javaClass) {
                    INSTANCE = ErrorDialog()
                }
            }
            return INSTANCE!!
        }

    }
}