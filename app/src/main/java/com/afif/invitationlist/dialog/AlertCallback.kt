package com.afif.invitationlist.dialog

import android.app.Dialog

interface AlertCallback{
    fun onButtonClicked(dialog: Dialog)
}