package com.afif.invitationlist.dialog

import android.app.Dialog

interface ConfirmationCallback{
    fun onButtonClicked()
}