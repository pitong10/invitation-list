package com.afif.invitationlist.fragments.tag

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.afif.invitationlist.R
import com.afif.invitationlist.base.AdapterListener
import com.afif.invitationlist.base.BaseFragment
import com.afif.invitationlist.databinding.FragmentTagSearchBinding
import com.afif.invitationlist.repository.entity.Tag

class TagSearchFragment : BaseFragment(R.layout.fragment_tag_search) {

    companion object {
        val REQUEST_KEY = "search_tag_key"
        val BUNDLE_KEY_TAG = "selected_tag"
    }

    private lateinit var binding: FragmentTagSearchBinding
    private val viewModel: TagSearchViewModel by viewModels { TagSearchViewModel.Factory(requireActivity().application) }
    private lateinit var tagAdapter: TagAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentTagSearchBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTitle(getString(R.string.txt_tag))
        setBackButton(true)

        tagAdapter = TagAdapter()
        binding.rvTag.apply {
            adapter = tagAdapter
            layoutManager = LinearLayoutManager(binding.root.context)
        }
        tagAdapter.listener = object : AdapterListener<Tag> {
            override fun onclick(item: Tag) {
                val bundle = Bundle().apply {
                    putParcelable(BUNDLE_KEY_TAG, item)
                }
                setFragmentResult(REQUEST_KEY, bundle)
                findNavController().navigateUp()
            }

            override fun onDeleteClick(item: Tag) {

            }

            override fun onChangeStatus(item: Tag) {

            }

        }

        viewModel.observableTagList.observe(viewLifecycleOwner) {
            tagAdapter.submitList(it)
        }

        binding.fabAddTag.setOnClickListener {
            findNavController().navigate(TagSearchFragmentDirections.searchTagToAddTag())
        }

        binding.etSearchTag.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(searchedText: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.observableTagListByName(searchedText.toString()).observe(viewLifecycleOwner) {
                    tagAdapter.submitList(it)
                }
            }

            override fun afterTextChanged(p0: Editable?) {
            }

        })
    }

}