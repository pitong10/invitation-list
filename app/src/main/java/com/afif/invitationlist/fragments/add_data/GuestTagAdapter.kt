package com.afif.invitationlist.fragments.add_data

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.afif.invitationlist.base.AdapterListener
import com.afif.invitationlist.databinding.ItemTagBinding

class GuestTagAdapter: ListAdapter<String, GuestTagAdapter.GuestTagViewHolder>(callback) {

    var listener: AdapterListener<String>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuestTagViewHolder {
        return GuestTagViewHolder(ItemTagBinding.inflate(LayoutInflater.from(parent.context), parent, false, ))
    }

    override fun onBindViewHolder(holder: GuestTagViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class GuestTagViewHolder(private val binding: ItemTagBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String) = with(binding.root) {
            binding.tvItemNameVariable.text = item
            binding.ivDeleteTag.setOnClickListener {
                listener?.onDeleteClick(item)
            }
        }
    }

    companion object {
        val callback = object : DiffUtil.ItemCallback<String>() {
            override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
                return oldItem == newItem
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
                return oldItem == newItem
            }

        }
    }

}