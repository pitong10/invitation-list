package com.afif.invitationlist.fragments.tag

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.afif.invitationlist.R
import com.afif.invitationlist.base.ErrorType
import com.afif.invitationlist.databinding.FragmentAddTagBinding
import com.afif.invitationlist.dialog.AlertCallback
import com.afif.invitationlist.extension.showAlertDialog
import com.afif.invitationlist.extension.toStringOrEmpty
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class AddTagFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentAddTagBinding
    private val viewModel: AddTagViewModel by viewModels { AddTagViewModel.Factory(requireActivity().application) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentAddTagBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ivActionEnd.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.btnSubmitTag.setOnClickListener {
            val tagName = binding.tieTagName.text.toString()
            viewModel.addTag(tagName)
        }

        viewModel.onError.observe(viewLifecycleOwner) {
            when (it.type) {
                ErrorType.ADD_DATA_FAILED -> {
                    showAlertDialog("Error", it.message.toStringOrEmpty(), "OK")
                }
                ErrorType.EMPTY_FIELD_TAG_NAME -> binding.tilTagName.error =
                    it.message.toStringOrEmpty()
                else -> {
                    showAlertDialog("Error", getString(R.string.message_error_unknown), "OK")
                }
            }
        }

        viewModel.isSuccess.observe(viewLifecycleOwner) {
            if (it) {
                val dialogCallback = object : AlertCallback {
                    override fun onButtonClicked(dialog: Dialog) {
                        findNavController().navigateUp()
                    }
                }

                showAlertDialog("Save Tag", "Tag data saved successfully", "OK", dialogCallback)
            }
        }
    }

}