package com.afif.invitationlist.fragments.add_data

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.afif.invitationlist.R
import com.afif.invitationlist.base.AdapterListener
import com.afif.invitationlist.base.BaseFragment
import com.afif.invitationlist.base.ErrorType
import com.afif.invitationlist.databinding.FragmentGuestBinding
import com.afif.invitationlist.dialog.AlertCallback
import com.afif.invitationlist.dialog.ConfirmationCallback
import com.afif.invitationlist.extension.showAlertDialog
import com.afif.invitationlist.extension.showConfirmationDialog
import com.afif.invitationlist.extension.toStringOrEmpty
import com.afif.invitationlist.fragments.tag.TagAdapter
import com.afif.invitationlist.fragments.tag.TagSearchFragment
import com.afif.invitationlist.repository.entity.Guest
import com.afif.invitationlist.repository.entity.Tag

class GuestFragment : BaseFragment(R.layout.fragment_guest) {

    companion object {
        private val GUEST_ARGS = "guest"
    }

    private lateinit var binding: FragmentGuestBinding
    private lateinit var tagAdapter: GuestTagAdapter
    private val viewModel: GuestViewModel by viewModels { GuestViewModel.Factory(requireActivity().application) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val guest = arguments?.getParcelable<Guest>(GUEST_ARGS)
        if (guest != null) {
            viewModel.set(guest)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentGuestBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTitle(getString(R.string.txt_guest))
        setBackButton(true)

        tagAdapter = GuestTagAdapter()
        binding.rvGuestTags.apply {
            adapter = tagAdapter
            layoutManager = LinearLayoutManager(binding.root.context)
        }
        tagAdapter.listener = object : AdapterListener<String> {
            override fun onclick(item: String) {

            }

            override fun onDeleteClick(item: String) {
                val listener = object : ConfirmationCallback {
                    override fun onButtonClicked() {
                        viewModel.removeGuestTag(item)
                        tagAdapter.notifyDataSetChanged()
                    }

                }
                showConfirmationDialog(
                    getString(R.string.txt_confirm),
                    getString(R.string.txt_message_delete_guest_tag_confirm),
                    listener
                )

            }

            override fun onChangeStatus(item: String) {

            }

        }

        setFragmentResultListener(TagSearchFragment.REQUEST_KEY) { _, bundle ->
            val tag = bundle.getParcelable<Tag>(TagSearchFragment.BUNDLE_KEY_TAG)
            if (tag != null) {
                viewModel.addNewGuestTag(tag.toString())
                tagAdapter.notifyDataSetChanged()
            }
        }

        viewModel.observableGuest.observe(viewLifecycleOwner) {
            bindView(it)
        }

        viewModel.observableGuestTags.observe(viewLifecycleOwner) {
            if (it.isEmpty())
                binding.tvNoData.visibility = View.VISIBLE
            else {
                binding.tvNoData.visibility = View.GONE
                tagAdapter.submitList(it)
            }

        }

        binding.btnSaveGuest.setOnClickListener {
            val name = binding.tieGuestName.text.toString()
            val notedStatsus = binding.cbIsChecked.isChecked
            viewModel.addGuest(name, notedStatsus)
        }

        viewModel.onError.observe(viewLifecycleOwner) {
            when (it.type) {
                ErrorType.ADD_DATA_FAILED -> {
                    showAlertDialog("Error", it.message.toStringOrEmpty(), "OK")
                }
                ErrorType.EMPTY_FIELD_GUEST_NAME -> binding.tilGuestName.error =
                    it.message.toStringOrEmpty()
                else -> {
                    showAlertDialog("Error", getString(R.string.message_error_unknown), "OK")
                }
            }
        }

        viewModel.isSuccess.observe(viewLifecycleOwner) {
            if (it) {
                val dialogCallback = object : AlertCallback {
                    override fun onButtonClicked(dialog: Dialog) {
                        findNavController().navigateUp()
                    }
                }

                showAlertDialog("Save Guest", "Guest data saved successfully", "OK", dialogCallback)
            }
        }

        binding.ivAddGuestTag.setOnClickListener {
            findNavController().navigate(GuestFragmentDirections.guestToSearchTag())
        }


    }

    private fun bindView(guest: Guest) {
        binding.tieGuestName.setText(guest.name)
        binding.cbIsChecked.isChecked = guest.status
    }
}