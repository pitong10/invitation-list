package com.afif.invitationlist.fragments.mainlist

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.afif.invitationlist.R
import com.afif.invitationlist.base.AdapterListener
import com.afif.invitationlist.base.BaseFragment
import com.afif.invitationlist.databinding.FragmentMainListBinding
import com.afif.invitationlist.dialog.ConfirmationCallback
import com.afif.invitationlist.extension.showConfirmationDialog
import com.afif.invitationlist.fragments.add_data.GuestAdapter
import com.afif.invitationlist.repository.entity.Guest

class MainListFragment : BaseFragment(R.layout.fragment_main_list) {

    private lateinit var binding: FragmentMainListBinding
    private lateinit var guestAdapter: GuestAdapter
    private val viewModel: MainListViewModel by viewModels { MainListViewModel.Factory(requireActivity().application) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentMainListBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setBackButton(false)
        setTitle(getString(R.string.app_name))

        guestAdapter = GuestAdapter()
        binding.rvGuest.apply {
            adapter = guestAdapter
            layoutManager = LinearLayoutManager(binding.root.context)
        }
        guestAdapter.listener = object : AdapterListener<Guest> {
            override fun onclick(item: Guest) {
                findNavController().navigate(MainListFragmentDirections.listToGuestEdit(item))
            }

            override fun onDeleteClick(item: Guest) {
                val listener = object : ConfirmationCallback {
                    override fun onButtonClicked() {
                        viewModel.removeGuest(item)
                        guestAdapter.notifyDataSetChanged()
                    }

                }
                showConfirmationDialog(
                    getString(R.string.txt_confirm),
                    getString(R.string.txt_message_delete_guest_confirm),
                    listener
                )
            }

            override fun onChangeStatus(item: Guest) {
                viewModel.updateStatusGuest(item)
                guestAdapter.notifyDataSetChanged()
            }

        }

        viewModel.observableGuestData.observe(viewLifecycleOwner) {
            guestAdapter.submitList(it)
            binding.tvTotalGuest.text = it.size.toString()
        }

        binding.fabAddGuest.setOnClickListener {
            findNavController().navigate(MainListFragmentDirections.listToGuestAdd())
        }

        binding.etSearchGuest.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(searchedText: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.observableGuestListByNameOrTag(searchedText.toString()).observe(viewLifecycleOwner) {
                    guestAdapter.submitList(it)
                    binding.tvTotalGuest.text = it.size.toString()
                }
            }

            override fun afterTextChanged(p0: Editable?) {
            }

        })
    }

}