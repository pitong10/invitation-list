package com.afif.invitationlist.fragments.tag

import android.app.Application
import androidx.lifecycle.*
import com.afif.invitationlist.R
import com.afif.invitationlist.base.AppError
import com.afif.invitationlist.base.BaseViewModel
import com.afif.invitationlist.base.ErrorType
import com.afif.invitationlist.repository.dao.GuestDao
import com.afif.invitationlist.repository.db.AppDatabase
import com.afif.invitationlist.repository.entity.Tag
import com.afif.invitationlist.repository.repository.TagRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddTagViewModel(application: Application): BaseViewModel() {

    private val repository: TagRepository
    private val application = application

    init {
        val tagDao = AppDatabase.getDatabase(application).tagDao()
        repository = TagRepository(tagDao)
    }

    fun addTag(tagName: String?) {
        if (tagName.isNullOrEmpty()) {
            onError.value = AppError(
                type = ErrorType.EMPTY_FIELD_TAG_NAME,
                message = application.getString(R.string.message_error_empty_field, "Tag Name")
            )
        }
        viewModelScope.launch(Dispatchers.IO) {
            val tag = Tag(
                id = 0,
                name = tagName.toString()
            )

            val id = repository.addTag(tag)
            if (id < 1) {
                onError.value = AppError(
                    type = ErrorType.ADD_DATA_FAILED,
                    message = application.getString(R.string.message_error_input_data,"Tag")
                )
                return@launch
            }

        }

        isSuccess.value = true
    }

    class Factory(private val application: Application) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return AddTagViewModel(application) as T
        }
    }
}