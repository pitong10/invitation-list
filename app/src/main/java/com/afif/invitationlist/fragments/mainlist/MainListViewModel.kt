package com.afif.invitationlist.fragments.mainlist

import android.app.Application
import androidx.lifecycle.*
import com.afif.invitationlist.fragments.tag.TagSearchViewModel
import com.afif.invitationlist.repository.dao.GuestDao
import com.afif.invitationlist.repository.db.AppDatabase
import com.afif.invitationlist.repository.entity.Guest
import com.afif.invitationlist.repository.repository.GuestRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainListViewModel(application: Application): AndroidViewModel(application) {

    val observableGuestData: LiveData<List<Guest>>
    private val repository: GuestRepository

    init {
        val guestDao = AppDatabase.getDatabase(application).guestDao()
        repository = GuestRepository(guestDao)
        observableGuestData = repository.readAllData
    }

    fun addGuest(guest: Guest) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addGuest(guest)
        }
    }

    fun removeGuest(guest: Guest) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteGuest(guest)
        }
    }

    fun updateStatusGuest(guest: Guest) {
        viewModelScope.launch(Dispatchers.IO) {
            guest.status = !guest.status
            repository.updateGuest(guest)
        }
    }

    fun observableGuestListByNameOrTag(query: String): LiveData<List<Guest>> {
        return repository.getGuestByNameOrTag(query)
    }

    class Factory(private val application: Application) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return MainListViewModel(application) as T
        }
    }
}