package com.afif.invitationlist.fragments.add_data

import android.app.Application
import androidx.lifecycle.*
import com.afif.invitationlist.R
import com.afif.invitationlist.base.AppError
import com.afif.invitationlist.base.BaseViewModel
import com.afif.invitationlist.base.ErrorType
import com.afif.invitationlist.extension.toStringOrEmpty
import com.afif.invitationlist.repository.db.AppDatabase
import com.afif.invitationlist.repository.entity.Guest
import com.afif.invitationlist.repository.repository.GuestRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GuestViewModel(application: Application) : BaseViewModel() {

    private val repository: GuestRepository
    private val application = application
    private var guestSelectedTags = mutableListOf<String>()

    private val mutableObservableGuest: MutableLiveData<Guest> = MutableLiveData()
    private val mutableObservableGuestTags: MutableLiveData<List<String>> = MutableLiveData()
    val observableGuest = mutableObservableGuest
    val observableGuestTags = mutableObservableGuestTags

    private var guestId = 0

    init {
        val dao = AppDatabase.getDatabase(application).guestDao()
        repository = GuestRepository(dao)
        mutableObservableGuestTags.value = guestSelectedTags
    }

    fun set(guest: Guest) {
        guestId = guest.id
        mutableObservableGuest.value = guest
        val tagList = guest.tags.split(",")
        guestSelectedTags = tagList.toMutableList()
        mutableObservableGuestTags.value = tagList
    }

    fun addNewGuestTag(tag: String) {
        guestSelectedTags.add(tag)
        mutableObservableGuestTags.value = guestSelectedTags
    }

    fun removeGuestTag(tag: String) {
        guestSelectedTags.remove(tag)
        mutableObservableGuestTags.value = guestSelectedTags
    }

    fun addGuest(name: String?, status: Boolean) {
        if (name.isNullOrEmpty()) {
            onError.value = AppError(
                type = ErrorType.EMPTY_FIELD_GUEST_NAME,
                message = application.getString(R.string.message_error_empty_field, "Name")
            )
        }
        viewModelScope.launch(Dispatchers.IO) {
            var guestTag = ""
            for (i in 0 until guestSelectedTags.size) {
                val separator = if (i != guestSelectedTags.size - 1) "," else ""
                guestTag += "${guestSelectedTags[i]}$separator"
            }
            val guest = Guest(
                id = guestId,
                name = name.toStringOrEmpty(),
                tags = guestTag,
                status = status
            )

            val id: Int = if (guest.id < 1) repository.addGuest(guest).toInt() else repository.updateGuest(guest)
            if (id < 1) {
                onError.value = AppError(
                    type = ErrorType.ADD_DATA_FAILED,
                    message = application.getString(R.string.message_error_input_data, "Guest")
                )
                return@launch
            }
        }

        isSuccess.value = true
    }


    class Factory(private val application: Application) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return GuestViewModel(application) as T
        }
    }
}