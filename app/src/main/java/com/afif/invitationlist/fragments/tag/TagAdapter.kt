package com.afif.invitationlist.fragments.tag

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.afif.invitationlist.base.AdapterListener
import com.afif.invitationlist.databinding.ItemTagBinding
import com.afif.invitationlist.repository.entity.Tag

class TagAdapter: ListAdapter<Tag, TagAdapter.KotaViewHolder>(callback) {

    var listener: AdapterListener<Tag>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KotaViewHolder {
        return KotaViewHolder(ItemTagBinding.inflate(LayoutInflater.from(parent.context), parent, false, ))
    }

    override fun onBindViewHolder(holder: KotaViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class KotaViewHolder(private val binding: ItemTagBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Tag) = with(binding.root) {
            binding.tvItemNameVariable.text = item.toString()

            binding.cardItemVariable.setOnClickListener {
                listener?.onclick(item)
            }
        }
    }

    companion object {
        val callback = object : DiffUtil.ItemCallback<Tag>() {
            override fun areItemsTheSame(oldItem: Tag, newItem: Tag): Boolean {
                return oldItem.id == newItem.id
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: Tag, newItem: Tag): Boolean {
                return oldItem == newItem
            }

        }
    }

}