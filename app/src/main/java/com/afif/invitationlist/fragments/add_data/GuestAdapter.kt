package com.afif.invitationlist.fragments.add_data

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.afif.invitationlist.R
import com.afif.invitationlist.base.AdapterListener
import com.afif.invitationlist.databinding.ItemGuestBinding
import com.afif.invitationlist.repository.entity.Guest

class GuestAdapter: ListAdapter<Guest, GuestAdapter.KotaViewHolder>(callback) {

    var listener: AdapterListener<Guest>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KotaViewHolder {
        return KotaViewHolder(ItemGuestBinding.inflate(LayoutInflater.from(parent.context), parent, false, ))
    }

    override fun onBindViewHolder(holder: KotaViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class KotaViewHolder(private val binding: ItemGuestBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Guest) = with(binding.root) {
            binding.tvGuestName.text = item.toString()
            binding.tvTagsGuest.text = item.tags

            val image = if (item.status)
                ContextCompat.getDrawable(context, R.drawable.ic_check)
            else
                ContextCompat.getDrawable(context, R.drawable.ic_uncheck)
            binding.ivStatusGuest.setImageDrawable(image)

            binding.ivStatusGuest.setOnClickListener {
                listener?.onChangeStatus(item)
            }
            binding.cardItemGuest.setOnClickListener {
                listener?.onclick(item)
            }
            binding.ivDeleteGuest.setOnClickListener {
                listener?.onDeleteClick(item)
            }
        }
    }

    companion object {
        val callback = object : DiffUtil.ItemCallback<Guest>() {
            override fun areItemsTheSame(oldItem: Guest, newItem: Guest): Boolean {
                return oldItem.id == newItem.id
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: Guest, newItem: Guest): Boolean {
                return oldItem == newItem
            }

        }
    }

}