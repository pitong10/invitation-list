package com.afif.invitationlist.fragments.tag

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.afif.invitationlist.base.BaseViewModel
import com.afif.invitationlist.repository.db.AppDatabase
import com.afif.invitationlist.repository.entity.Tag
import com.afif.invitationlist.repository.repository.TagRepository

class TagSearchViewModel(application: Application): BaseViewModel() {

    private val repository: TagRepository
    val observableTagList: LiveData<List<Tag>>

    init {
        val tagDao = AppDatabase.getDatabase(application).tagDao()
        repository = TagRepository(tagDao)
        observableTagList = repository.readAllData
    }

    fun observableTagListByName(tagName: String): LiveData<List<Tag>> {
        return repository.getTagByName(tagName)
    }

    class Factory(private val application: Application) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return TagSearchViewModel(application) as T
        }
    }

}