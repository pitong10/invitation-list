package com.afif.invitationlist.extension

fun String?.toStringOrEmpty() : String {
    return this ?: ""
}