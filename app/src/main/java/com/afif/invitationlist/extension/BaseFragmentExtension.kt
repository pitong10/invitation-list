package com.afif.invitationlist.extension

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import com.afif.invitationlist.base.BaseFragment
import com.afif.invitationlist.dialog.AlertCallback
import com.afif.invitationlist.dialog.AlertDialog
import com.afif.invitationlist.dialog.ConfirmationCallback
import com.afif.invitationlist.dialog.LoadingDialog

fun BaseFragment.showLoading() {
    val transaction = childFragmentManager.beginTransaction()
    val dialog = childFragmentManager.findFragmentByTag("Loading")
    if (dialog?.isAdded == true) return
    loadingDialog = LoadingDialog()
    loadingDialog.isCancelable = false
    try {
        loadingDialog.show(transaction, "Loading")
    } catch (e: IllegalStateException) {
        e.printStackTrace()
    }
}

fun BaseFragment.dismissLoading() {
    if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED) &&
        loadingDialog.dialog != null &&
        loadingDialog.dialog!!.isShowing &&
        !loadingDialog.isRemoving
    ) loadingDialog.dismiss()
}

fun BaseFragment.cancelLoading() {
    loadingDialog.dismissAllowingStateLoss()
}

fun Fragment.showConfirmationDialog(
    title: String,
    message: String,
    listener: ConfirmationCallback
) {
    val builder = android.app.AlertDialog.Builder(requireContext())
    builder.setPositiveButton("Yes") { _, _ ->
        listener.onButtonClicked()
    }
    builder.setNegativeButton("No") { _, _ ->
    }
    builder.setTitle(title)
    builder.setMessage(message)
    builder.create().show()
}

fun Fragment.showAlertDialog(
    title: String,
    message: String,
    button: String,
    listener: AlertCallback? = null
) {
    val dialog = AlertDialog.newInstance(title, message, button)
    listener?.let { dialog.dismissListener = it }
    dialog.isCancelable = false
    dialog.show(childFragmentManager, "AlertDialog")
}
