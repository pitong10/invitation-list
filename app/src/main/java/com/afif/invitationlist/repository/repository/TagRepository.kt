package com.afif.invitationlist.repository.repository

import androidx.lifecycle.LiveData
import com.afif.invitationlist.repository.dao.GuestDao
import com.afif.invitationlist.repository.dao.TagDao
import com.afif.invitationlist.repository.entity.Guest
import com.afif.invitationlist.repository.entity.Tag

class TagRepository(private val dao: TagDao) {

    val readAllData: LiveData<List<Tag>> = dao.readAllTag()

    suspend fun addTag(tag: Tag) : Long {
        return dao.addTag(tag)
    }

    suspend fun updateTag(tag: Tag) {
        dao.updateTag(tag)
    }

    suspend fun deleteTag(tag: Tag) {
        dao.deleteTag(tag)
    }

    fun getTagByName(name: String): LiveData<List<Tag>> {
        val nameQuery = "%$name%"
        return dao.readTagByName(nameQuery)
    }

}
