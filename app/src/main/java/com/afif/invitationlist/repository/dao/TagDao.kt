package com.afif.invitationlist.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.afif.invitationlist.repository.entity.Guest
import com.afif.invitationlist.repository.entity.Tag

@Dao
interface TagDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addTag(tag: Tag): Long

    @Update
    suspend fun updateTag(tag: Tag)

    @Delete
    suspend fun deleteTag(tag: Tag)

    @Query("SELECT * FROM tag")
    fun readAllTag(): LiveData<List<Tag>>

    @Query("SELECT * FROM tag WHERE name LIKE :name")
    fun readTagByName(name: String): LiveData<List<Tag>>

}