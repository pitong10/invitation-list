package com.afif.invitationlist.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.afif.invitationlist.repository.entity.Guest

@Dao
interface GuestDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addGuest(guest: Guest): Long

    @Update
    suspend fun updateGuest(guest: Guest): Int

    @Delete
    suspend fun deleteGuest(guest: Guest)

    @Query("SELECT * FROM guest")
    fun readAllGuest(): LiveData<List<Guest>>

    @Query("SELECT * FROM guest WHERE name LIKE :query OR tags LIKE :query")
    fun readGuestByNameOrTag(query: String): LiveData<List<Guest>>

}