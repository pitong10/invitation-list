package com.afif.invitationlist.repository.repository

import androidx.lifecycle.LiveData
import com.afif.invitationlist.repository.dao.GuestDao
import com.afif.invitationlist.repository.entity.Guest

class GuestRepository(private val dao: GuestDao) {

    val readAllData: LiveData<List<Guest>> = dao.readAllGuest()

    suspend fun addGuest(guest: Guest) : Long {
        return dao.addGuest(guest)
    }

    suspend fun updateGuest(guest: Guest): Int {
        return dao.updateGuest(guest)
    }

    suspend fun deleteGuest(guest: Guest) {
        dao.deleteGuest(guest)
    }

    fun getGuestByNameOrTag(query: String): LiveData<List<Guest>> {
        val finalQuery = "%$query%"
        return dao.readGuestByNameOrTag(finalQuery)
    }

}
