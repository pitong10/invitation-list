package com.afif.invitationlist.repository.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.afif.invitationlist.repository.dao.GuestDao
import com.afif.invitationlist.repository.dao.TagDao
import com.afif.invitationlist.repository.entity.Guest
import com.afif.invitationlist.repository.entity.Tag

@Database(
    entities =
    [
        Guest::class,
        Tag::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun guestDao(): GuestDao
    abstract fun tagDao(): TagDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null


        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "invitation_db"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }
}