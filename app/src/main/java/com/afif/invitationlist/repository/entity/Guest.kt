package com.afif.invitationlist.repository.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "guest")
data class Guest(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var name: String,
    var tags: String,
    var status: Boolean,
) : Parcelable {
    override fun toString(): String {
        return name
    }
}
