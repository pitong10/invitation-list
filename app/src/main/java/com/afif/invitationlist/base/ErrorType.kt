package com.afif.invitationlist.base

enum class ErrorType {
    INTERNET_ERROR,
    SERVER_ERROR,
    VERSION_ERROR,

    //Login
    LOGIN_USER_INVALID,
    LOGIN_PASSWORD_INVALID,
    LOGIN_NOT_FOUND,
    LOGIN_UNAUTHORIZED,

    // Transaction
    ADD_DATA_FAILED,
    UPDATE_DATA_FAILED,
    DELETE_DATA_FAILED,

    // Form Error
    EMPTY_FIELD_TAG_NAME,
    EMPTY_FIELD_GUEST_NAME
}

