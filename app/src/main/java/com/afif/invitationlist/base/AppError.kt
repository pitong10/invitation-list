package com.afif.invitationlist.base

import com.afif.invitationlist.base.ErrorType

data class AppError(val type: ErrorType, val message: String?)
