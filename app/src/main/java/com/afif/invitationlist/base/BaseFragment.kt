package com.afif.invitationlist.base

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.afif.invitationlist.R
import com.afif.invitationlist.dialog.LoadingDialog

open class BaseFragment(layoutId: Int): Fragment(layoutId), View.OnClickListener {

    var loadingDialog = LoadingDialog()
    var onRightButtonListener: ImageViewListener? = null
    var onSubmitButtonListener: ImageViewListener? = null

    override fun onClick(view: View?) {
        when(view?.id) {
            R.id.iv_action_toolbar -> Navigation.findNavController(view).navigateUp()
//            R.id.iv_action_end_toolbar -> Navigation.findNavController(view).navigateUp()
        }
    }

    open fun setBackButton(isVisible: Boolean = false) {
        view?.let {
            val backBtn = it.findViewById<ImageView>(R.id.iv_action_toolbar)
            if (isVisible) {
                backBtn.visibility = View.VISIBLE
                backBtn.setOnClickListener(this)
            } else backBtn.visibility = View.GONE
        }
    }

    open fun setTitle(title: String) {
        view?.let {
            val toolbarTitle = it.findViewById<TextView>(R.id.tv_toolbar_title)
            toolbarTitle.text = title
        }
    }

}