package com.afif.invitationlist.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel() {

    val onError: SingleLiveEvent<AppError> = SingleLiveEvent()
    val isLoading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val isSuccess: SingleLiveEvent<Boolean> = SingleLiveEvent()
}