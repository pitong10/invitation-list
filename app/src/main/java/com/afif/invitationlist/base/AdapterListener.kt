package com.afif.invitationlist.base

interface AdapterListener<T> {
    fun onclick(item: T)
    fun onDeleteClick(item: T)
    fun onChangeStatus(item: T)
}
